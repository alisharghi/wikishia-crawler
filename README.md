```
fa.WikiShia.net Crawler for Educational purpose

{
    python : 'Backend',
    BeautifulSoup4 : 'Parse Html Page',
    Elastic-Search : 'Indexing',
    MongoDB : ['Backup','Save Articles Link to Resume Crawling'],
    ReactJS : 'Frontend'
}


clone:
    git clone https://gitlab.com/alisharghi/wikishia-crawler.git
    
Before run crawler , You must run Elastic And Mongo in Your pc

Run Crawler:
    cd ./wikishia-crawler
    python ./crawler.py

Run ReactJs:
    cd ./front
    npm i
    npm start
    
Go To : http://127.0.0.1:3000 and search Your Query
```
