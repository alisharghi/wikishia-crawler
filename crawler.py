import requests
import os
from bs4 import BeautifulSoup
import logging
from elasticsearch import Elasticsearch
import pymongo
import urllib.request


def connect_to_elastic():
    global visited,es
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    if es.ping():
        print('Connected to Elastic')
        create_elastic_index(es)
        try:
            data = returndata(es)
            data = data['hits']['hits']
            print('number of saved page : ' + str(len(data)))
            visited = len(data)
        except:
            print('empty')
    else:
        print('Could not connect to Elastic!')
    return es

def connect_to_mongodb():
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["wikishia"]
    try:
        urllib.request.urlopen('http://127.0.0.1:27017/')
        print('Connected to MongoDB')
    except:
        print('Could not connect to MongoDB!')
    return mydb
 

#get All data from elastic
def returndata(es):
    doc = {
        'from' : 0,
        'size' : 10000,
        'query': {
            'match_all' : {}
        }
    }
    res = es.search(index="wikishia", doc_type='wikishia', body=doc)
    return res


#Delete By Index
def delete_elastic(es):
    try:
        es.indices.delete(index='wikishia', ignore=[400, 404])
        print('Elastic Data Deleted')
    except:
        print('No Data Available for Deleting')

def delete_mongo(mycol):
    try:
        mycol.drop()
        print('MongoDB Data Deleted')
    except:
        print('No Data Available for Deleting')

def copy_to_db(es,mycol):
    try:
        data = returndata(es)
        data = data['hits']['hits']
        delete_mongo(mycol)
        for article in data:
            mycol.insert_one(article['_source'])
        print('Elastic Data Saved in MongoDB')
    except:
        print('Error!')


def retrive_from_db(es,mycol):
    try:
        for article in mycol.find():
            rec = {
                'title':article['title'],
                'link':article['link'],
                'desc':article['desc']
            }
            store_record(rec)
        print('Restored From Backup')
    except:
        print('Error!')


#create index for articles if not exist
def create_elastic_index(es, index_name='wikishia'):
    created = False
    # index settings
    settings = {
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 0
        },
        "mappings": {
            "members": {
                "dynamic": "strict",
                "properties": {
                    "title": {
                        "type": "text"
                    },
                    "link": {
                        "type": "text"
                    },
                    "desc": {
                        "type": "text"
                    },
                }
            }
        }
    }

    try:
        # Ignore 400 means to ignore "Index Already Exist" error.
        es.indices.create(index=index_name, ignore=400, body=settings)
        created = True
    except Exception as ex:
        print(str(ex))
    finally:
        return created


def store_record(record):
    try:
        outcome = es.index(index='wikishia', doc_type='wikishia', body=record)
        # mycol.insert_one(record)
    except Exception as ex:
        print('Error in indexing data')
        print(str(ex))


#extract article content and links
def parsearticle(article):
    global visited
    global total_page
    db_link_list.update_one({"link":article},{"$set": {"visited":True}})
    page = requests.get(url + article)
    soup = BeautifulSoup(page.text,'html.parser')
    redirect = soup.find(class_='mw-redirectedfrom')
    #distinct page
    if redirect == None:
        title = soup.h1.text
        link = article
        desc = ''
        content = soup.find(class_='mw-content-rtl')
        for text in content.find_all('p'):
            desc = desc + '''
            ''' + text.text
        res = {
            'title':title,
            'link':link,
            'desc':desc
        }
        store_record(res)
        visited = visited + 1
        total_page = db_link_list.estimated_document_count()
        print('Progress : ' + str(visited) + '/' + str(total_page))
        article = soup.find(class_='mw-body')
        for article_link in article.find_all('a'):
            if article_link.get('title') != None:
                href = article_link.get('href')
                i = 0
                for check in db_link_list.find({"link":href}):
                    i = i+1
                    break
                if(i==0):
                    db_link_list.insert_one({
                        'link' : href,
                        'visited' : False
                    })

        #break

def run_elastic(es):
    #first crawl page
    alphabet_link_list = []
    page = requests.get(url + '/view/%D9%88%DB%8C%DA%A9%DB%8C_%D8%B4%DB%8C%D8%B9%D9%87:%D9%81%D9%87%D8%B1%D8%B3%D8%AA_%D8%B3%D8%B1%DB%8C%D8%B9')

    soup = BeautifulSoup(page.text , 'html.parser')
    alphabet = soup.find(class_='plainlinks')
    for link in alphabet.find_all('a'):
        alphabet_link_list.append(link.get('href'))



    for link in alphabet_link_list:
        page = requests.get(url + link)
        soup = BeautifulSoup(page.text , 'html.parser')
        article = soup.find(class_='mw-allpages-body')
        #list of articles link in alphabet list
        for article_link in article.find_all('a'):
            href = article_link.get('href')
            if db_link_list.find_one({"link":href}) == None:
                db_link_list.insert_one({
                    'link' : href,
                    'visited' : False
                })
                parsearticle(href)
        #     break
        # break

    for link in db_link_list.find({"visited":False}):
        parsearticle(link['link'])

def remove_dublicates(es,mycol):
    total_url = mycol.estimated_document_count()
    distinct_url = mycol.distinct('link')
    if(len(distinct_url)!=total_url):
        for link in distinct_url:
            dis = mydb['wikishia'].find({"link":link})
            i=0
            dub = None
            for href in dis:
                if i != 0:
                    dub = href['link']
                i = i+1
            if(i>1):
                j = 0
                for duplicate in mydb['wikishia'].find({"link":dub}):
                    if(j>0):
                        mydb['wikishia'].delete_one({"link":dub})
                    j = j+1
        print(str(total_url-len(distinct_url)) + ' Data Removed From MongoDB')
        print('Deleting Elastic Data')
        delete_elastic(es)
        print('Copy From MongoDB to Elastic')
        retrive_from_db(es,mycol)
    else:
        print('Your Data is Clean')


def menu():
    os.system('clear')
    ch = int(input('''
    Please Enter A Number From Below : 
    1-Check Connection to Elastic Server
    2-Check Connection to MongoDB Server for Backup
    3-Delete Elastic Cache
    4-Delete Backup
    5-Backup
    6-Recovery from Backup
    7-Run Crawler and save into Elastic Server
    8-Remove Dublicates URLs From MongoDB
    '''))
    if ch==1 :
        connect_to_elastic()
    elif ch==2 :
        connect_to_mongodb()
    elif ch==3 :
        delete_elastic(es)
    elif ch==4 :
        delete_mongo(mycol)
    elif ch==5 : 
        copy_to_db(es,mycol)
    elif ch==6 :
        retrive_from_db(es,mycol)
    elif ch==7 :
        run_elastic(es)
    elif ch==8 :
        remove_dublicates(es,mycol)
    else:
        print('Invalid Input')


logging.basicConfig(level=logging.ERROR)
#wikishia url
url = 'http://fa.wikishia.net'
#number of page indexed
visited = 0
#total page to crawl
total_page = 0
mydb = connect_to_mongodb()
mycol = mydb["wikishia"]
db_link_list = mydb['list']
total_page = db_link_list.estimated_document_count()
es = connect_to_elastic()
menu()