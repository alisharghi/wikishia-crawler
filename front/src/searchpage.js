import React from 'react'
import Particles from 'react-particles-js'
const particlesOptions = {
    particles: {
        number: {
          value: 150,
          density: {
            enable: true,
            value_area: 800
          }
        },
        color: {
          value: '#fff'
        },
        size: {
          value: 2
        },
        line_linked: {
          enable: true,
          distance: 150,
          color: '#fff',
          opacity: 1,
          width: 1
        },
    },
    interactivity : {
        detect_on : 'windows',
        events : {
            onhover : {
                enable : true,
                mode : 'repulse'
            }
        }
    }
}

class search extends React.Component {
    constructor(){
        super()
        this.state = {
            query : '',
            error: ''
        }
    }

    handle_enter = (e)=>{
        if(e.charCode===13 || e.target.id==='search'){
            this.setState({error:''})
            if(this.state.query===''){
                this.setState({error:'Please Enter a Value to Search'})
            }
            else{
                let route = '/search/' + this.state.query + '/1'
                this.props.history.push(route)
            }
        }
    }

    handle_text = (e)=>{
        this.setState({query:e.target.value})
    }

    render(){
        return(
            <div className="App">
                <div className="background">
                <Particles className="particles" params={particlesOptions}/>
                    <div className="column">
                        <div className="row">
                            <input value={this.state.query} onChange={this.handle_text} 
                                onKeyPress={this.handle_enter} placeholder='جستجو ...' 
                                className='inputStyle' />
                            <button className='buttonStyle'
                                id='search' onClick={this.handle_enter}>جستجو</button>
                        </div>
                        <p>{this.state.error}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default search