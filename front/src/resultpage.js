import React from 'react'
import logo from './logo.svg'

class result extends React.Component {
    constructor(props){
        super()
        this.state = {
            query : props.match.params.query,
            error : '',
            loading : true,
            response : '',
            page : props.match.params.page,
            total : 1
        }
    }

    componentDidMount(){
        this.handle_request()
    }

    componentDidUpdate(prevProps) {
        const locationChanged = this.props.location !== prevProps.location;
        if(locationChanged){
            this.handle_request()
        }
    }

    handle_request = async()=>{
        await this.setState({
            error:'',
            loading:true,
            page : this.props.match.params.page,
            query : this.props.match.params.query,
            response : '',
            total : 1
        })
        let url = 'http://localhost:9200/wikishia/_search'
        let from = this.state.page-1
        from = from*10
        let query = {
            "multi_match":{
                "query": this.state.query,
                "fields":["title","desc"],
                //catch error spelling
                "fuzziness":"AUTO"
            }
        }
        let post = {
            query: query,
            size: 10,
            from: from
        }
        fetch(url,{
            method : 'POST',
            body : JSON.stringify(post),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then((value)=>{
            value.json().then((data)=>{
                this.setState({
                    response:data.hits,
                    loading : false
                })
                if(data.hits.hits.length ===0){
                    this.setState({
                        error : '404',
                        loading : false
                    })
                }
                this.setState({
                    total : data.hits.total.value/10
                })
            })
        }).catch(()=>{
            this.setState({
                error:'Error While Connecting To Server',
                loading:false
            })
        })
    }

    handle_enter = (e)=>{
        if(e.charCode===13 || e.target.id==='search'){
            let route = '/search/'
            if(this.state.query===''){
                this.props.history.push(route)
            }
            else {
                route = '/search/' + this.state.query + '/1'
                this.props.history.push(route)
                this.handle_request()
            }
        }
    }

    handle_text = (e)=>{
        this.setState({
            query:e.target.value
        })
    }


    render(){
        let content
        if(this.state.loading){
            content = <img src={logo} className="App-logo" alt="logo" />
        }
        else {
            if(this.state.error){
                content = <p style={{fontSize:50}}>{this.state.error}</p>
            }
            else {
                content = this.state.response.hits.map(element => {
                    let url = "http://fa.wikishia.net" + element._source.link
                    return <div key={url} className='anim' style={{paddingLeft:32,paddingRight:32}}>
                        <a href={url} target='_blank' className='linkStyle'>{element._source.title}</a>
                        <p>{element._source.desc.slice(0,350)} ...</p>
                        <p style={{direction:"ltr",fontSize:13,color:'#00ff77'}}>{decodeURI(url)}</p>
                        <hr></hr>
                     </div>
                })
            }
        }
        let next_page = parseInt(this.state.page) + 1
        let next = "http://localhost:3000/search/" + this.props.match.params.query + '/' + next_page
        let prev_page = parseInt(this.state.page) - 1
        let prev = "http://localhost:3000/search/" + this.props.match.params.query + '/' + prev_page
        return(
            <div className="App">
                <header className="search-bar">
                    <input value={this.state.query} onChange={this.handle_text} onKeyPress={this.handle_enter} placeholder='جستجو ...' 
                        className='inputStyle' />
                    <button className='buttonStyle' 
                        id='search' onClick={this.handle_enter}>جستجو</button>
                        {this.state.total?<text style={{paddingRight:32,fontSize:14}}>{this.state.total*10} نتیجه </text>:null}
                </header>
                <div className="background">
                    <div className='column' style={{textAlign:"right"}}>
                        {content}
                    </div>
                </div>
                <div className='footer'>
                    {this.state.page>1?<a href={prev} className='linkStyle anim'>صفحه قبل</a>:null}
                    {this.state.total>this.state.page?<a href={next} className='linkStyle anim'>صفحه بعد</a>:null}
                </div>
            </div>
        )
    }
}

export default result