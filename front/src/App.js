import React from 'react';
import logo from './logo.svg';

export default class App extends React.Component{
  
  componentDidMount(){
    setTimeout(()=>{
      this.props.history.replace('/search')
    },3000)
  }

  render(){
    return (
      <div className="App">
        <header className="background">
          <div className="column">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Welcome to My Search Engine
            </p>
            <p>
              Programmed By Ali Sharghi With 
            </p><p>
              ["ReactJS" , "Python" , "Elastic"]
            </p>
          </div>
        </header>
      </div>
  );
  }
}